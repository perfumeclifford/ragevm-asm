# VM assembler v1.9.3

Assembler for ragevm. Processes assembler files listed in a package description file and
produces an vm executable file.

(VM and disassembler coming soon in another repo.)

*Please copy/fork me on github!*

## building

Download the repo to your favorite UNIX system, then just 'make' in the toplevel directory.

    $ make

This will actually build the assembler and run it on the provided example assembly.

Please check the resulting vm executable with unix 'file' command.
The format used is actually a common unix format and should be recognized on most systems.

    $ file vm.out
