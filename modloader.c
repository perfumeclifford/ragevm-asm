#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "common.h"
#include "libdec.c"

static void load(FILE *f, FILE *o){
  char path[128];
  while(fgets(path, arrsz(path), f)){
    if('#'==path[0] or '\n'==path[0]) continue;
    unless(0==path[0]) path[strlen(path)-1]=0;
    FILE *e = fopen(path, "r");
    unless(e){ warn("cannot open: %s\n", path); return; }
    dec(e,o);
    assert(0==fclose(e));
  }
}
int main(int ac, char *av[]){
  if(1==ac) load(stdin, stdout);
  else
    for(unsigned i=1; i<(unsigned)ac; ++i){
      FILE *f=fopen(av[i], "r");
      load(f, stdout);
      assert(0==fclose(f));
    }
  return 0;
}
