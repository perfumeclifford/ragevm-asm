#define and &&
#define or ||
#define not !
#define unless(x) if(!(x))
#define arrsz(a) (sizeof(a)/sizeof(a[0]))

#define warn(fmt,...) do{fprintf(stderr, fmt, __VA_ARGS__); }while(0)
#define die(fmt,...) do{fprintf(stderr, fmt, __VA_ARGS__); exit(1);}while(0)
typedef unsigned char byte;
