CFLAGS=-Wall -Wextra -Werror -Wno-unused-parameter
CFLAGS+=-O2

vm.out: modloader package.mod
	./modloader package.mod >$@
