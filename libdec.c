#include "op2.c"
static void decode_op(char const* s, unsigned len, FILE *o){
  assert(3==len);
  unsigned iop=arrsz(op);
  for(unsigned i=0; i<arrsz(op); ++i){
    if(0==memcmp(op[i], s, 2)){ iop=i; break; }
  }
  assert(iop<arrsz(op));
  unsigned low=16;
  if('0'<=s[2] and s[2]<='9') low=s[2]-'0';
  else if('a'<=s[2] and s[2]<='f') low=s[2]-'a'+10;
  else die("illegal hex digit:%c\n", s[2]);
  assert(low<16);
  byte const b = (iop<<4)|low;
  fputc_unlocked(b, o);
}
static void decode_arg(char const* s, unsigned len, FILE *o){
  char d[8];
  assert(len<arrsz(d));
  memmove(d, s, len);
  d[len]=0;
  byte b=0;
  switch(s[0]){
    default:
      b=strtoul(d, NULL, 0);
      break;
    case 'h':
      b=strtoul(d+1, NULL, 16);
      break;
    case '\'':
      b=s[1];
      break;
  }
  fputc_unlocked(b, o);
}
static void dec(FILE *f, FILE *o){
  char line[128];
  while(fgets(line, arrsz(line), f)){
    unsigned const len = strlen(line);
    assert(len>0);
    line[len-1]=' ';
    char *e;
    for(char *s=line;(e=strchr(s,' '));s=e+1){
      if(s==line){
        decode_op(s, e-s, o);
      }else{
        decode_arg(s, e-s, o);
      }
    }
  }
}
