#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "common.h"
#include "libdec.c"

int main(int ac, char *av[]){
  if(1==ac) dec(stdin, stdout);
  else
    for(unsigned i=1; i<(unsigned)ac; ++i){
      FILE *f=fopen(av[i], "r");
      dec(f, stdout);
      assert(0==fclose(f));
    }
  return 0;
}
